const parse = require('url-parse');
const crypto = require("crypto");
const fs = require('fs');
const toml = require('toml');
const b58 = require('b58');
const fetch = require("node-fetch");
const glob = require("glob");
const StellarSdk = require('stellar-sdk');
const path = require('path');
const tqdm = require('tqdm');
const server = new StellarSdk.Server('https://dex.digitalworld.global');
const streamServer = new StellarSdk.Server('https://dex.digitalworld.global');
var lastCursor = "now"; // or load where you left off
var isRunning = false;

/* 
  Gets a list of the assets currently available on Stellar.
  
    Returns:
        array: of unique URLs pointing to the `stellar.toml` files that describe the assets.
*/
const getAssetList = async (testnet=false) => {
  var links = [];
  var completed = [];
  var cursor = "";

  var pages = 0;
  var limit = 200;
  while (cursor !== null && !completed.includes(cursor)) {
    completed.push(cursor);
    await server.assets().cursor(cursor).limit(limit).call().then(function(response) {

      response.records.forEach(asset => {
        var link = asset["_links"]["toml"]["href"];
        if (link !== "") {
          links.push(link);
        }
        cursor = asset["paging_token"];
      });

      pages += 1;
    });
  }
  console.log(`Queried ${pages} pages with a limit of ${limit} assets per page.`);

  return links;
}

const fetch_url = async (url) => {
  var uri = parse(url);

  var target = path.join("../orgs", `${uri.hostname}.json`);

  if (!fs.existsSync("../orgs/")) {
    fs.mkdirSync("../orgs/")
  }

  if (!fs.existsSync(target)) {
    await fetch(url).then(async function(response) {
      if (response.status === 200) {
        var data = await response.text();

        try {
          fs.writeFileSync(target, data);
        } catch (err) {
          console.error(err);
        }
      }
    });
  }

  return target;
}

const fetchIcon = async (url) => {
  var uri = parse(url);

  var f = path.parse(uri.toString()).name
  var ext = path.parse(uri.toString()).ext

  var target = path.join("../icons", `${uri.hostname}_${f}${ext}`);

  if (!fs.existsSync("../icons/")) {
    fs.mkdirSync("../icons/");
  }

  if (!fs.existsSync(target)) {
    await fetch(url).then(async function(response) {
      if (response.status === 200) {
        var data = await response.blob();
        const arrayBuffer = await data.arrayBuffer();
        const buffer = Buffer.from(arrayBuffer);

        try {
          fs.writeFileSync(target, buffer);
        } catch (err) {
          console.error(err);
        }
      }
    });
  }

  return target;
};

const parseAssetParallel = async (urls) => {
  const requests = urls.map((url) => fetch_url(url));
  const targets = await Promise.all(requests);
  return targets;
};

const fetchIconParallel = async (urls) => {
  const requests = urls.map((url) => fetchIcon(url));
  const targets = await Promise.all(requests);
  return targets;
};

/*
  Sanitizes the given `stellar.toml` metadata to conform to the expected structure.
    
  .. note:: These sanitizing rituals are created by trial and error when `gson`
      whines about parsing the JSON file. 
*/
const sanitizeAssets = (asset_toml) => {
  if (asset_toml["DOCUMENTATION"]) {
    const docs = asset_toml["DOCUMENTATION"];
    if (Array.isArray(docs))
      asset_toml["DOCUMENTATION"] = docs[0];
  }

  if (asset_toml["CURRENCIES"]) {
    const currencies = asset_toml["CURRENCIES"];
    if (Array.isArray(currencies)) {
      currencies.forEach(c => {
        const addresses = c["collateral_addresses"];
        if (addresses && !Arrays.isArray(addresses)) {
            c["collateral_addresses"] = [addresses];
        }

        const sigs = c["collateral_address_signatures"];
        if (sigs && !Arrays.isArray(sigs)) {
          c["collateral_address_signatures"] = [sigs];
        }

        const fixedNumber = c["fixed_number"];
        if (fixedNumber && fixedNumber.toString().includes(",")) {
          c["fixed_number"] = fixedNumber.toString().replace(",", "");
        }
      });
    }
    asset_toml["CURRENCIES"] = currencies;
  }

  return asset_toml;
};

const parseFile = (target) => {
  var result = null;
  if (fs.existsSync(target)) {
    try {
      const data = fs.readFileSync(target, 'utf8');
      result = toml.parse(data);
      result = sanitizeAssets(result);
    } catch (e) {
      console.error("Parsing error on line " + e.line + ", column " + e.column + ": " + e.message);
    }
  }
  return result;
};

/*
  Returns a hash of all the assets available in the asset list. This
  is the hash of the `;`-separated string of ordered, unique `ACCOUNTS`
  in the `stellar.toml` files retrieved from remote servers.
*/
const getAssetHash = (assets) => {
  const accounts = [];
  assets.forEach(s => {
    if (s['ACCOUNTS'] != undefined)
      accounts.push(s['ACCOUNTS']);
    else
      accounts.push([]);
  });

  accounts.sort();
  const m = crypto.createHash("sha256");
  m.update(accounts.join(";"), "utf-8");

  return b58.encode(m.digest());
};

const run = async () => {
  const unique = await getAssetList(false);

  const files = await parseAssetParallel(unique);

  const assets = [];
  glob.sync("../orgs/*.json").forEach(f => {
    var st = parseFile(f);
    if (st !== null) {
      assets.push(st);
    }
  });

  const exported = {    
    "updated": Date.now(),
    "assets": assets,
    "hash": getAssetHash(assets)
  }

  const short = {
    "updated": exported["updated"],
    "hash": exported["hash"]
  }
  console.log(short);

  fs.writeFileSync("../digital-world-hash.json", JSON.stringify(exported));
  fs.writeFileSync("../digital-world-orgs.json", JSON.stringify(exported));

  const icons = new Set();
  assets.forEach(asset => {
    if (asset["CURRENCIES"]) {
      asset["CURRENCIES"].forEach(currency => {
        if (currency["code"] && currency["issuer"] !== "" && !currency["issuer"].includes("$") && currency["image"]) {
          icons.add(currency["image"]);
        }
      });
    }
  });

  assets.forEach(asset => {  
    if (asset["DOCUMENTATION"]["ORG_LOGO"]) {
      icons.add(asset["DOCUMENTATION"]["ORG_LOGO"]);
    }
  });
  
  fetchIconParallel(Array.from(icons));

  const unwound = new Map();
  const clean_assets = [];
  assets.forEach(asset => {
    if (asset["CURRENCIES"]) {
      asset["CURRENCIES"].forEach(currency => {
        if (currency["issuer"] !== "" && !currency["issuer"].includes("$") && currency["code"]) {
          const pair = [[currency['code'], currency['issuer']]];
          unwound.set(pair, asset);
          if (!clean_assets.includes(asset)) {
            clean_assets.push(asset);
          }
        }
      });
    }
  });

  const caps = new Map();
  for(let [code, issuer] of tqdm(Array.from(unwound.keys()))) {
    const r = await server.assets().forCode(code).forIssuer(issuer).limit(10).call();
    const pair = [code, issuer];
    caps.set(pair, r);
  }

  const combined = [];
  for (const [key, r] of caps) {
    if (r.records.length > 0) {
      combined.push({
        "code": key[0][0],
        "issuer": key[0][1],
        "amount": r.records[0]["amount"],
        "accounts": r.records[0]["num_accounts"]
      });
    }
  } 

  combined.sort((a,b) => b.accounts - a.accounts);

  fs.writeFileSync("../digital-world-assets.json", JSON.stringify(combined));

  const time = new Date().toLocaleString('en-US', {timeZone: 'America/Denver'});
  console.log(`Completed at ${time} Mountain Time`)

  isRunning = false;
};

run();

const txHandler = function (txResponse) {
  if (isRunning) {
    console.log("Script is running. Waiting 60 seconds before trying again");
    setTimeout(txHandler, 60000);
  } else {
    isRunning = true;
    console.log("Script is not running. Continuing to run() function");

    // Waiting 5 seconds before running script to allow asset to get setup completely
    setTimeout(run, 5000);
  }
};

const errHandler = (error) => {
  console.error("EventSource failed:", error);
}

const es = streamServer.payments()
  .forAccount('GAER3XPLI3ODLWCFFQJKKWM7PJ5FFJAWW6XEU2Y7E6K6VGSFTJCNJYQ2')
  .cursor(lastCursor)
  .stream({
    onmessage: txHandler,
    onerror: errHandler
  });

process.on('SIGINT', function() {
  console.log("\nCaught interrupt signal. Closing stream channel and exiting");
  es();
  process.exit();
});